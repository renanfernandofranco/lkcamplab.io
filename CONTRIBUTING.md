# Clone the repository:
```
$ git clone git@gitlab.com:lkcamp/lkcamp.gitlab.io.git

$ cd lkcamp.gitlab.io

$ git submodule update --init --recursive

```

# To run the site on your computer:

## Install hugo:

```
$ sudo dnf install hugo # redhat like

$ sudo apt-get install hugo # debian like
```

## Run the web page on your computer:

```
$ hugo serve
```